import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from copenhagen_cards_tests import functions
from copenhagen_cards_tests.const import link
from copenhagen_cards_tests.driverinit import driver


class secondtest(unittest.TestCase):
    def test_more_than_zero_save(self) -> None:
        """Проверка на выгоду больше нуля"""

        driver.get(link)

        functions.findElementByClassName("coi-banner__accept").click()
        functions.findElemsWithDelayByXpath('//*[@id="location-item-list"]/li[5]/span').click()
        functions.findElemsWithDelayByXpath('//*[@id="location-item-list"]/li[6]/span').click()
        functions.findElemsWithDelayByXpath('//*[@id="location-item-list"]/li[7]/span').click()
        functions.findElementById("calculator-step-1-button").click()
        functions.findElementByXpath("//*[@id='step-2']/div[2]/div[1]/div/span[2]").click()
        functions.findButtonsWithDelayById("calculator-step-2-button").click()
        functions.findButtonsWithDelayById("calculator-step-3-button").click()
        price = functions.findElementByClassName("calculator__savings-amount")
        not self.assertTrue("0.00 EUR" in price.text)
        driver.quit()