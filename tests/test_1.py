import unittest
from selenium.webdriver.common.by import By
from selenium import webdriver
from copenhagen_cards_tests import functions
from copenhagen_cards_tests.const import link
from copenhagen_cards_tests.driverinit import driver
class firsttest(unittest.TestCase):

    def test_zero_money_save(self) -> None:
        """Проверка на выгоду равную нулю"""
        driver.get(link)
        functions.findElementByClassName("coi-banner__accept").click()
        functions.findElemsWithDelayByXpath("//*[@id='location-item-list']/li[1]/span").click()
        functions.findElementById("calculator-step-1-button").click()
        functions.findElemsWithDelayByXpath("//*[@id='step-2']/div[2]/div[1]/div/span[2]").click()
        functions.findButtonsWithDelayById("calculator-step-2-button").click()
        functions.findButtonsWithDelayById("calculator-step-3-button").click()
        price = functions.findElementByClassName("calculator__savings-amount")
        print(price)
        self.assertTrue("0.00 EUR" in price.text)






