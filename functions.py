from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from copenhagen_cards_tests.driverinit import driver
def findElementById(id):
    elem = driver.find_element(By.ID, id)
    return elem
def findElementByClassName(classname):
    elem = driver.find_element(By.CLASS_NAME, classname)
    return elem

def findElementByXpath(xpath):
    elem = driver.find_element(By.XPATH, xpath)
    return elem

def findElementByCSSSelector(selector):
    elem = driver.find_element(By.CSS_SELECTOR, selector)
    return elem

def findButtonsWithDelayById(id):
    elem = WebDriverWait(driver, 12).until(
        EC.element_to_be_clickable((By.ID, id)))
    return elem
def findElemsWithDelayByClassName(classname):
    elem = WebDriverWait(driver, 12).until(
        EC.element_to_be_clickable((By.CLASS_NAME, classname)))
    return elem

def findElemsWithDelayByXpath(xpath):
    elem = WebDriverWait(driver, 12).until(
        EC.element_to_be_clickable((By.XPATH, xpath)))
    return elem
