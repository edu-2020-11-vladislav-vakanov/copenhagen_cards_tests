# copenhagen_cards_tests

his project was made to test https://copenhagencard.com/attractions.

## Getting started

To start this test code, you need:

1. To install pycharm or other IDE;
2. To install pip;
3. To open terminal and write -> (pip install -r requirements.txt)
4. Then you need to start this project and see result.
 

        button1 = WebDriverWait(driver, 12).until(
            EC.element_to_be_clickable((By.CLASS_NAME, "calculator-step-2-button")))
        button1.click()


        functions.findElementByIdAndClick("calculator-step-3-button")

        self.assertTrue("0.00 EUR" in driver.page_source)
